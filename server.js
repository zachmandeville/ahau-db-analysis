const Config = require('ssb-config/defaults')
const pull = require('pull-stream')
const { ahau: env } = require('ahau-env')
const { promisify: p } = require('util')

const AHAU_AOTEAROA = '101.98.39.231:8088:@QhiiCWBuxwbHs0bDFZoAgxGLhOlX88FQvvDd4aKD4Os=.ed25519~vdszvGYDU+3kHC+nOPtvZsgrM2HKfHRIfiuW/69cTFc=' // production

module.exports = async function Server (opts = {}) {
  const appName = 'ahau-db-analysis'
  const config = Config(appName, {
    port: 5005,
    caps: env.caps,
    ...opts
  })

  const stack = require('secret-stack')({ caps: env.caps })
    .use(require('ssb-db'))
    .use(require('ssb-backlinks'))
    .use(require('ssb-query'))

    .use(require('ssb-conn'))
    .use(require('ssb-lan'))
    .use(require('ssb-replicate'))
    .use(require('ssb-friends'))
    .use(require('ssb-invite'))

    .use(require('ssb-profile'))

  // console.log('config:')
  // console.log(JSON.stringify(config, null, 2))

  const ssb = stack(config)

  await friendPataka(ssb)

  // Log connections
  ssb.on('rpc:connect', (remote) => console.log('connected:', remote.id))
  // pull(ssb.conn.peers(), pull.log())

  // Log replication
  pull(
    ssb.createLogStream({ live: true, old: false }),
    pull.filter(m => m.value && m.value.author !== ssb.id),
    pull.drain(m => {
      console.log(`replicated ${m.value.author}, seq: ${m.value.sequence}`)
    })
  )

  // Log indexing progress
  let target = 0
  setInterval(
    () => {
      const current = ssb.progress()
      if (current.indexes.target > target) console.log(current)
      target = current.indexes.target
    },
    10000
  )

  return ssb
}

async function friendPataka (ssb) {
  const data = await p(ssb.friends.hops)()
  // console.log(data)

  if (Object.keys(data).length > 1) return null // we have friends we follow!

  console.log('using pataka invite...')

  return p(ssb.invite.accept)(AHAU_AOTEAROA)
    .catch((err) => {
      console.log('could not join pataka')
      throw err
    })
}
